package kisai.nofi;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class register extends AppCompatActivity {
    private boolean finish = false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_regist);

        final EditText name = (EditText) findViewById(R.id.regist_name); 
        
        Button regist = (Button) findViewById(R.id.regist_submit);
        regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uname = name.getText().toString();
                if(uname.isEmpty() || uname.equals("")){
                    name.setError(getString(R.string.error_sign_reg_name_empty));
                    name.requestFocus();
                    return;
                }else{
                    String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    String email = FirebaseAuth.getInstance().getCurrentUser().getEmail();
                    String date = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
                    User newUser = new User(UID, email, uname, "user", date);
                    FirebaseDatabase.getInstance().getReference(getString(R.string.db_user))
                            .child(UID).setValue(newUser).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(register.this, getString(R.string.sign_reg_success), Toast.LENGTH_SHORT).show();
                                finish = true;
                                finish();
                            }
                        }
                    });
                }
            }
        });
    }
    @Override
    public void onBackPressed() {
        FirebaseAuth.getInstance().signOut();
        super.onBackPressed();
    }
}
