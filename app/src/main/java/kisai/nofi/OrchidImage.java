package kisai.nofi;

import java.io.Serializable;

public class OrchidImage implements Serializable {
    private String id_orchid_post, filename;

    public OrchidImage(){}

    public OrchidImage(String id_orchid_post, String filename) {
        this.id_orchid_post = id_orchid_post;
        this.filename = filename;
    }

    public String getId_orchid_post() {
        return id_orchid_post;
    }

    public void setId_orchid_post(String id_orchid_post) {
        this.id_orchid_post = id_orchid_post;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

}
