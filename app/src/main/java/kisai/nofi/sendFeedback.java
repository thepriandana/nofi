package kisai.nofi;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import kisai.nofi.Admin.createOrchidInfo;

public class sendFeedback extends AppCompatActivity {
    ArrayList<Uri> imageuri;
    LinearLayout imagelist;
    private int rcPermission, rcLocal;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback_send);
        imageuri = new ArrayList<Uri>();

        rcPermission = Integer.parseInt(getString(R.string.RC_PERMISSION));
        rcLocal = Integer.parseInt(getString(R.string.RC_LOCAL));


        EditText desc = (EditText) findViewById(R.id.fs_status);
        imagelist = (LinearLayout) findViewById(R.id.fs_imagelist);
        Button addimage = (Button) findViewById(R.id.fs_addimage);
        addimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findPhoto();
            }
        });


        Button submit = (Button) findViewById(R.id.fs_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (desc.getText().toString().equals("") || desc.getText().toString().isEmpty()) {
                    desc.setError(getString(R.string.cu_empty_desc));
                    desc.requestFocus();
                    return;
                }else if(imageuri.size()==0 || imageuri.isEmpty()){
                    Toast.makeText(sendFeedback.this, getString(R.string.create_empty_image), Toast.LENGTH_SHORT).show();
                }else{
                    submitFeedback(desc.getText().toString());
                }
            }
        });
    }

    void submitFeedback(String descs){
        //check if the user already login first
        ProgressDialog todo = new ProgressDialog(sendFeedback.this);
        todo.setCancelable(false);
        todo.setMessage(getString(R.string.loading_wait));
        todo.show();
        //Create orchid post first
        String id_post = FirebaseDatabase.getInstance().getReference(getString(R.string.db_feedback_post)).push().getKey();
        String date = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
        OrchidPost newPost = new OrchidPost(id_post, FirebaseAuth.getInstance().getCurrentUser().getUid(), descs, date);
        FirebaseDatabase.getInstance().getReference(getString(R.string.db_feedback_post)).child(id_post).setValue(newPost).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                int x = 0;
                for(Uri uwi: imageuri){
                    int finalX = x;
                    StorageReference fotoRef = FirebaseStorage.getInstance().getReference(getString(R.string.db_feedback_pics)).child(id_post+"_"+x);
                    fotoRef.putFile(uwi).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            fotoRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    OrchidImage oi = new OrchidImage(id_post, uri.toString());
                                    String id_image = FirebaseDatabase.getInstance().getReference(getString(R.string.db_feedback_img)).push().getKey();
                                    FirebaseDatabase.getInstance().getReference(getString(R.string.db_feedback_img)).child(id_image).setValue(oi).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            todo.setMessage(getString(R.string.loading_wait)+" "+ String.valueOf(finalX+1) +"/"+String.valueOf(imageuri.size()));
                                        }
                                    });
                                }
                            });
                        }
                    });
                    x += 1;
                }
                todo.dismiss();
                if(x == imageuri.size()){
                    Toast.makeText(sendFeedback.this, getString(R.string.feedback_success), Toast.LENGTH_SHORT).show();
                    finish();
                }else{

                }
            }
        });
        //final StorageReference fotoRef = FirebaseStorage.getInstance().getReference(getString(R.string.db_orchid_img)).child(id_orchid);

    }

    void findPhoto(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(sendFeedback.this,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(sendFeedback.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, rcPermission);
        }else{
            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, rcLocal);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == rcLocal && resultCode == RESULT_OK && data != null) {
            try {
                imageuri.add(data.getData());
                ImageView imgView = new ImageView(sendFeedback.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(300, 300);
                lp.setMargins(10,10,10,10);
                imgView.setLayoutParams(lp);
                imgView.setImageURI(data.getData());
                imagelist.addView(imgView);
            }catch (Exception e){

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == rcPermission){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, rcLocal);
            } else {
                Toast.makeText(this, getString(R.string.error_permission), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
