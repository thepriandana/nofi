package kisai.nofi;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


interface sync_progress {
    public void onResponseReceived();
}
public abstract class sync extends AsyncTask<String, Integer, String> implements  sync_progress{
    private Context context;
    private PowerManager.WakeLock mWakeLock;
    private ProgressDialog dialog;
    String targetF;

    public abstract void onResponseReceived();
    public sync(Context context, String target_file) {
        super();
        this.context = context;
        targetF = target_file;
        dialog = new ProgressDialog(context);

        dialog.setMessage(context.getString(R.string.sync_progress));
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setCancelable(true);
        // reference to instance to use inside listener
        final sync me = this;
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                me.cancel(true);
            }
        });
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                getClass().getName());
        mWakeLock.acquire(120*60*1000L /*120 minutes*/);
        dialog.show();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        dialog.setProgress(values[0]);
        dialog.setMax(100);
        dialog.setIndeterminate(false);
    }

    @Override
    protected void onPostExecute(String s) {
        mWakeLock.release();
        dialog.dismiss();
        if (s != null){
            Log.d("xxx", "error "+s);
            Toast.makeText(context,context.getString(R.string.sync_error)+" "+s, Toast.LENGTH_LONG).show();
        }else{
            //remove target and rename tmp
            try {
                File tmp_file = new File(context.getFilesDir(), String.format("tmp_%s", targetF));
                File target_file = new File(context.getFilesDir(), targetF);
                if(tmp_file.renameTo(target_file)){
                    onResponseReceived();
                }
            }catch (Exception e){

            }
        }
    }

    @Override
    protected String doInBackground(String... urls) {
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(urls[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage();
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            int fileLength = connection.getContentLength();

            // download the file
            input = connection.getInputStream();
            //save on different name first
            output = context.openFileOutput(String.format("tmp_%s", targetF), Context.MODE_PRIVATE);

            byte[] data = new byte[fileLength];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                if (isCancelled()) {
                    input.close();
                    return null;
                }
                total += count;
                // publishing the progress....
                if (fileLength > 0) // only if total length is known
                    publishProgress((int) (total * 100 / fileLength));
                output.write(data, 0, count);
            }
        } catch (Exception e) {
            return e.toString();
        } finally {
            try {
                if (output != null){
                    output.flush();
                    output.close();
                }
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
        return null;
    }
}
