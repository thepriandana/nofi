package kisai.nofi;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import kisai.nofi.Adapter.orchidAdapter;
import kisai.nofi.Admin.createOrchidInfo;
import kisai.nofi.Admin.editOrchidInfo;
import kisai.nofi.Admin.removeOrchidInfo;

public class readOrchidInfo extends AppCompatActivity {
    TextView name, fragance, temperature, branch, growth, description, sunlight;
    OrchidSpecies tmp_ocs;
    private DatabaseReference userDB, orchidDB;
    private FirebaseDatabase firebaseDatabase;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orchid_read);

        name = (TextView) findViewById(R.id.o_name);
        fragance = (TextView) findViewById(R.id.o_fragance);
        temperature = (TextView) findViewById(R.id.o_temperature);
        branch = (TextView) findViewById(R.id.o_branch);
        growth = (TextView) findViewById(R.id.o_growth);
        description = (TextView) findViewById(R.id.o_description);
        sunlight = (TextView) findViewById(R.id.o_sunlight);
        ImageView image = (ImageView) findViewById(R.id.o_image);

        FloatingActionButton o_edit = (FloatingActionButton) findViewById(R.id.o_edit);
        FloatingActionButton o_remove = (FloatingActionButton) findViewById(R.id.o_remove);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            OrchidSpecies ocs= (OrchidSpecies) extras.get("ocs");
            tmp_ocs = ocs;
            setData(ocs);
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            firebaseDatabase = FirebaseDatabase.getInstance();
            if (user != null) {
                //Check the user level
                userDB = firebaseDatabase.getReference(getString(R.string.db_user));
                userDB.child(user.getUid()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        User current = snapshot.getValue(User.class);
                        if(current.getLevel().equals("admin")){
                            o_edit.setVisibility(View.VISIBLE);
                            o_edit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intentEdit = new Intent(readOrchidInfo.this, editOrchidInfo.class);
                                    intentEdit.putExtra("ocs", tmp_ocs);
                                    startActivity(intentEdit);
                                }
                            });
                            o_remove.setVisibility(View.VISIBLE);
                            o_remove.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intentRemove = new Intent(readOrchidInfo.this, removeOrchidInfo.class);
                                    intentRemove.putExtra("ocs", tmp_ocs);
                                    startActivity(intentRemove);
                                }
                            });
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
            orchidDB = firebaseDatabase.getReference(getString(R.string.db_orchid));
            orchidDB.child(ocs.getId_orchid_species()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    tmp_ocs = (OrchidSpecies) snapshot.getValue(OrchidSpecies.class);
                    if(tmp_ocs != null && !tmp_ocs.getImageUri().isEmpty()){
                        Picasso.get().load(tmp_ocs.getImageUri()).into(image);
                    }
                    if(tmp_ocs != null){
                        setData(tmp_ocs);
                    }else{
                        finish();
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

        }
    }

    void setData(OrchidSpecies ocs){
        name.setText(ocs.getName_orchid());
        fragance.setText(getResources().getStringArray(R.array.crud_fragance)[(ocs.getFragrance() ? 0:1)]);
        temperature.setText(ocs.getTemperature());
        branch.setText(ocs.getBranchType());
        growth.setText(ocs.getGrowthMedium());
        sunlight.setText(ocs.getSunlight());
        description.setText(ocs.getDescription());
    }
}
