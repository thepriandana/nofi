package kisai.nofi.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import kisai.nofi.OrchidImage;
import kisai.nofi.OrchidPost;
import kisai.nofi.OrchidSpecies;
import kisai.nofi.R;
import kisai.nofi.listOrchid;
import kisai.nofi.sendFeedback;
import kisai.nofi.viewFeedback;

public class feedbackAdapter extends BaseAdapter {
    private ArrayList<OrchidPost> op;
    ArrayList<ArrayList<OrchidImage>> oi;
    private Context mcontext;
    public feedbackAdapter(Context mcontext, ArrayList<OrchidPost> op, ArrayList<ArrayList<OrchidImage>> oi){
        this.mcontext = mcontext;
        this.op = op;
        this.oi = oi;

    }

    @Override
    public int getCount() {
        return op.size();
    }

    @Override
    public Object getItem(int position) {
        return op.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mcontext).inflate(R.layout.feedback_view, parent, false);
        }
        TextView desc = (TextView) convertView.findViewById(R.id.fs_desc);
        desc.setText(op.get(position).getStatus());

        LinearLayout listImage = (LinearLayout) convertView.findViewById(R.id.fs_imagelist);
//        Log.d("xxx", oi.get(position).toString());
        for(OrchidImage orchidImage: oi.get(position)){
            ImageView imgView = new ImageView(mcontext);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(300, 300);
            lp.setMargins(10,10,10,10);
            imgView.setLayoutParams(lp);
            Picasso.get().load(orchidImage.getFilename()).into(imgView);
            listImage.addView(imgView);
        }

        ImageView del = (ImageView) convertView.findViewById(R.id.fs_delete);
        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeValue(position);
            }
        });
        return  convertView;
    }

    private void removeValue(int position){
        new AlertDialog.Builder(mcontext)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(mcontext.getString(R.string.view_del_win))
                .setMessage(mcontext.getString(R.string.view_del))
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //remove the child first
                        FirebaseDatabase.getInstance().getReference(mcontext.getString(R.string.db_feedback_img)).orderByChild("id_orchid_post")
                                .equalTo(op.get(position).getId_orchid_post()).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                if(snapshot.exists()){
                                    for (DataSnapshot sn : snapshot.getChildren()) {
                                        FirebaseDatabase.getInstance().
                                                getReference(mcontext.getString(R.string.db_feedback_img))
                                                .child(sn.getKey()).removeValue();
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });

                        FirebaseDatabase.getInstance().
                                getReference(mcontext.getString(R.string.db_feedback_post))
                                .child(op.get(position).getId_orchid_post()).removeValue();
                        Intent listOrchidIntent = new Intent(mcontext, viewFeedback.class);
                        listOrchidIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mcontext.startActivity(listOrchidIntent);
                        Toast.makeText(mcontext, mcontext.getString(R.string.remove_success), Toast.LENGTH_SHORT).show();
                    }

                })
                .show();
    }
}
