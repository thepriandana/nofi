package kisai.nofi.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import kisai.nofi.OrchidSpecies;
import kisai.nofi.R;
import kisai.nofi.readOrchidInfo;

public class orchidAdapter extends BaseAdapter {
    ArrayList<OrchidSpecies> orchid_species;
    Context context;
    public orchidAdapter(Context context, ArrayList<OrchidSpecies> os){
        orchid_species = os;
        this.context = context;
    }
    @Override
    public int getCount() {
        return orchid_species.size();
    }

    @Override
    public Object getItem(int position) {
        return orchid_species.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.orchid_rv_base, parent, false);
        }
        ImageView thumbail = (ImageView) convertView.findViewById(R.id.orchid_rv_image);
        if(!orchid_species.get(position).getImageUri().isEmpty()){
            thumbail.setImageURI(Uri.parse(orchid_species.get(position).getImageUri()));
            Picasso.get().load(orchid_species.get(position).getImageUri()).into(thumbail);
        }
        TextView name = (TextView) convertView.findViewById(R.id.orchid_rv_name);
        name.setText(orchid_species.get(position).getName_orchid());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ro = new Intent(context, readOrchidInfo.class);
                ro.putExtra("ocs", orchid_species.get(position));
                context.startActivity(ro);
            }
        });
        return convertView;
    }
}