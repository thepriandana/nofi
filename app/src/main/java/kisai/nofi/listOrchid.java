package kisai.nofi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import kisai.nofi.Adapter.orchidAdapter;
import kisai.nofi.Admin.createOrchidInfo;
import kisai.nofi.Admin.editOrchidInfo;

public class listOrchid extends AppCompatActivity {
    ListView orchid_lv;
    ArrayList<OrchidSpecies> specieslist;
    private DatabaseReference userDB, orchidDB;
    private FirebaseDatabase firebaseDatabase;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orchid_view);
        firebaseDatabase = FirebaseDatabase.getInstance();
        isAdmin();
        listOrchidInfo();
    }
    private void setupSearch(){
        SearchView sv = (SearchView) findViewById(R.id.o_search);
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchOrchid(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(TextUtils.isEmpty(newText)){
                    orchid_lv.setAdapter(new orchidAdapter(listOrchid.this, specieslist));
                }else{
                    searchOrchid(newText);
                }
                return true;
            }
        });
    }

    private void searchOrchid(String orchidname){
        ArrayList<OrchidSpecies> tmp_ocs = new ArrayList<OrchidSpecies>();
        for (OrchidSpecies ocs : specieslist) {
            if(ocs.getName_orchid().toLowerCase().contains(orchidname.toLowerCase())){
                tmp_ocs.add(ocs);
            }
        }
        orchid_lv.setAdapter(new orchidAdapter(listOrchid.this, tmp_ocs));
    }
    private void listOrchidInfo(){
        try {
            ProgressDialog progressDialog = new ProgressDialog(listOrchid.this);
            progressDialog.setMessage(getString(R.string.loading_wait));
            progressDialog.show();
            orchid_lv = (ListView) findViewById(R.id.o_listview);
            orchidDB = firebaseDatabase.getReference(getString(R.string.db_orchid));
            orchidDB.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    specieslist = new ArrayList<OrchidSpecies>();
                    for (DataSnapshot sn : snapshot.getChildren()) {
                        OrchidSpecies orchid = sn.getValue(OrchidSpecies.class);
                        specieslist.add(orchid);
                    }
                    orchid_lv.setAdapter(new orchidAdapter(listOrchid.this, specieslist));
                    progressDialog.dismiss();
                    setupSearch();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }catch (Exception e){

        }
    }
    private void isAdmin(){
        FloatingActionButton create = (FloatingActionButton) findViewById(R.id.o_create);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            userDB = firebaseDatabase.getReference(getString(R.string.db_user));
            userDB.child(user.getUid()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    User current = snapshot.getValue(User.class);
                    if(current.getLevel().equals("admin")){
                        create.setVisibility(View.VISIBLE);
                        create.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(listOrchid.this, createOrchidInfo.class));
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
    }
}
