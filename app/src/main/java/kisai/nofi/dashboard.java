package kisai.nofi;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Surface;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Objects;

import kisai.nofi.Scan.scanLive;
import kisai.nofi.Scan.scanLocal;
import kisai.nofi.TensorFlow.TensorClassifier;

public class dashboard extends AppCompatActivity {
    private int rcPermission, rcLocal, rcSign;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private GoogleSignInClient mGoogleSignInClient;
    private TextView signIn, signOut, viewfeedback, scan_live, sync_button, tx_name;
    private LinearLayout listOrchid;
    private DatabaseReference userDB, modelDB;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);
        rcPermission = Integer.parseInt(getString(R.string.RC_PERMISSION));
        rcLocal = Integer.parseInt(getString(R.string.RC_LOCAL));
        rcSign = Integer.parseInt(getString(R.string.RC_SIGN));

        tx_name = (TextView) findViewById(R.id.dashboard_name);

        TextView scan_local = (TextView) findViewById(R.id.scan_local);
        scan_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanLocal();
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.app_oath))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(dashboard.this, gso);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();

        signIn = (TextView) findViewById(R.id.sign_in);
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

        signOut = (TextView) findViewById(R.id.sign_out);
        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               signOut();
            }
        });

        listOrchid = (LinearLayout) findViewById(R.id.listOrchid);
        listOrchid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(dashboard.this, listOrchid.class));
            }
        });

        viewfeedback = (TextView) findViewById(R.id.view_feedback);
        viewfeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(dashboard.this, viewFeedback.class));
            }
        });

        scan_live = (TextView) findViewById(R.id.scan_live);
        scan_live.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(dashboard.this, scanLive.class));
            }
        });

        sync_button = (TextView) findViewById(R.id.sync);
        sync_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                syncModel();
            }
        });
    }
    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser account = firebaseAuth.getCurrentUser();
        updateUI(account);
    }
    public void scanLocal(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(dashboard.this,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(dashboard.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, rcPermission);
        }else{
            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, rcLocal);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == rcPermission){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, rcLocal);
            } else {
                Toast.makeText(this, getString(R.string.error_permission), Toast.LENGTH_SHORT).show();
            }
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == rcLocal && resultCode == RESULT_OK && data != null) {
            Uri imageUri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                TensorClassifier classifier = TensorClassifier.create(this, TensorClassifier.Device.CPU, 1);
                ArrayList<TensorClassifier.Recognition> results = classifier.recognizeImage(bitmap, getScreenOrientation());
                //send the result list into new activity
                Intent resultActivity = new Intent(this, scanLocal.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("resultList", results);
                resultActivity.putExtra("resultImage", imageUri);
                resultActivity.putExtras(bundle);
                startActivity(resultActivity);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (requestCode == rcSign) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                Log.d("NOFI", "firebaseAuthWithGoogle:" + account.getId());
                firebaseAuthWithGoogle(account.getIdToken());
            } catch (ApiException e) {
                Log.w("NOFI", "Google sign in failed", e);
                updateUI(null);
            }
        }
    }
    private void firebaseAuthWithGoogle(String idToken) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("NOFI", "signInWithCredential:success");
                            Toast.makeText(dashboard.this, getString(R.string.sign_in_success), Toast.LENGTH_SHORT).show();
                            final FirebaseUser user = firebaseAuth.getCurrentUser();
                            updateUI(user);

                            userDB = firebaseDatabase.getReference(getString(R.string.db_user));
                            userDB.child(user.getUid()).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if(!snapshot.exists()){
                                        Intent regist = new Intent(dashboard.this, register.class);
                                        startActivity(regist);
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("NOFI", "signInWithCredential:failure", task.getException());
                            Toast.makeText(dashboard.this, getString(R.string.error_sign_in), Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                    }
                });
    }
    private void updateUI(FirebaseUser account) {
        if (account != null) {
            signIn.setVisibility(View.GONE);
            signOut.setVisibility(View.VISIBLE);
            viewfeedback.setVisibility(View.VISIBLE);
            userDB = firebaseDatabase.getReference(getString(R.string.db_user));
            userDB.child(account.getUid()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if(snapshot.child("name_user").getValue() == null){

                    }else{
                        String uname =  Objects.requireNonNull(snapshot.child("name_user").getValue()).toString();
                        tx_name.setText("Hi, "+uname+"!");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }else{
            signIn.setVisibility(View.VISIBLE);
            signOut.setVisibility(View.GONE);
            viewfeedback.setVisibility(View.INVISIBLE);
            tx_name.setText("Hi, Guest!");
        }
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, rcSign);
    }
    private void signOut() {
        // Firebase sign out
        firebaseAuth.signOut();
        // Google sign out
        mGoogleSignInClient.signOut().addOnCompleteListener(this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(dashboard.this, getString(R.string.sign_out_success), Toast.LENGTH_SHORT).show();
                        updateUI(null);
                    }
                });
    }
    private int getScreenOrientation() {
        switch (getWindowManager().getDefaultDisplay().getRotation()) {
            case Surface.ROTATION_270:
                return 270;
            case Surface.ROTATION_180:
                return 180;
            case Surface.ROTATION_90:
                return 90;
            default:
                return 0;
        }
    }
    private void syncModel(){
        modelDB = firebaseDatabase.getReference(getString(R.string.db_model));
        modelDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    String currentV = Objects.requireNonNull(snapshot.child("version").getValue()).toString();
                    String urlmodel = Objects.requireNonNull(snapshot.child("models").getValue()).toString();
                    String urllabel = Objects.requireNonNull(snapshot.child("labels").getValue()).toString();
                    if(readTexts("version.txt", false).trim().equals(currentV.trim())){
                        //already new
                        Toast.makeText(dashboard.this, getString(R.string.sync_fullfil), Toast.LENGTH_SHORT).show();
                    }else{
                        //download latest
                        if(!urlmodel.isEmpty()){
                            sync downloadLabel = new sync(dashboard.this, "labels.txt"){
                                @Override
                                public void onResponseReceived() {
                                    try {
                                        OutputStreamWriter outputWriter;
                                        FileOutputStream fos = openFileOutput("version.txt", Context.MODE_PRIVATE);
                                        outputWriter = new OutputStreamWriter(fos);
                                        outputWriter.write(currentV);
                                        outputWriter.close();
                                        fos.close();
                                        Toast.makeText(dashboard.this, getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
                                    }catch (Exception e){

                                    }
                                }
                            };

                            sync downloadModel = new sync(dashboard.this, "model.tflite") {
                                @Override
                                public void onResponseReceived() {
                                    downloadLabel.execute(urllabel);
                                }
                            };
                            downloadModel.execute(urlmodel);
                        }


                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    private String readTexts(String files, Boolean isAsset){
        StringBuilder stringBuilder = new StringBuilder();
        try {
            InputStreamReader inputStreamReader;
            if(isAsset){
                inputStreamReader = new InputStreamReader(getAssets().open(files));
            }else{
                inputStreamReader = new InputStreamReader(openFileInput(files));
            }
            BufferedReader reader = new BufferedReader(inputStreamReader);

            String line = reader.readLine();
            while (line != null) {
                stringBuilder.append(line).append('\n');
                line = reader.readLine();
            }
            return  stringBuilder.toString();
        } catch (Exception e) {
            // Error occurred when opening raw file for reading.
            return ""+e.toString();
        }
    }
}
