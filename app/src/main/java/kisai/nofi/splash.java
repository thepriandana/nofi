package kisai.nofi;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class splash extends AppCompatActivity {
    Thread background;
    LinearLayout sl_center, sl_right, sl_exist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        sl_center = (LinearLayout) findViewById(R.id.sl_center);
        sl_right = (LinearLayout) findViewById(R.id.sl_right);
        sl_exist = (LinearLayout) findViewById(R.id.sl_exist);

        background = new Thread() {
            public void run() {
                try {
                    sl_center.setVisibility(View.GONE);
                    sl_right.setVisibility(View.GONE);
                    sl_exist.setVisibility(View.VISIBLE);
                    sleep(1000);
                    startActivity(new Intent(splash.this, dashboard.class));
                    finish();
                } catch (Exception e) {

                }
            }
        };
        checkIFModelPlace();
    }
    private void checkIFModelPlace(){
        File file = new File(getFilesDir(), "version.txt");
        if(file.exists()){
            background.start();
        }else{
            try {
                OutputStreamWriter outputWriter;
                //write the model first
                FileOutputStream fos = openFileOutput("model.tflite", Context.MODE_PRIVATE);
                InputStream is = getAssets().open("model.tflite");
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();
                fos.write(buffer);
                fos.close();
                //create the labels
                fos = openFileOutput("labels.txt", Context.MODE_PRIVATE);
                outputWriter=new OutputStreamWriter(fos);
                outputWriter.write(readTexts("labels.txt", true));
                outputWriter.close();
                fos.close();
                //Create current version
                fos = openFileOutput("version.txt", Context.MODE_PRIVATE);
                outputWriter=new OutputStreamWriter(fos);
                outputWriter.write(getString(R.string.model_ver_default));
                outputWriter.close();
                fos.close();
                sl_right.setVisibility(View.VISIBLE);
                sl_center.setVisibility(View.VISIBLE);
                TextView continu = (TextView) findViewById(R.id.sl_continue);
                continu.setOnClickListener(v -> {
                    Toast.makeText(splash.this, "First model added!", Toast.LENGTH_SHORT).show();
                    Intent splash = new Intent(splash.this, splash.class);
                    splash.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(splash);
                });
            }catch (Exception e){
                Toast.makeText(splash.this, getString(R.string.error_base_write), Toast.LENGTH_SHORT).show();
            }

        }
    }
    private String readTexts(String files, Boolean isAsset){
        StringBuilder stringBuilder = new StringBuilder();
        try {
            InputStreamReader inputStreamReader;
            if(isAsset){
                inputStreamReader = new InputStreamReader(getAssets().open(files));
            }else{
                inputStreamReader = new InputStreamReader(openFileInput(files));
            }
            BufferedReader reader = new BufferedReader(inputStreamReader);

            String line = reader.readLine();
            while (line != null) {
                stringBuilder.append(line).append('\n');
                line = reader.readLine();
            }
            return  stringBuilder.toString();
        } catch (Exception e) {
            // Error occurred when opening raw file for reading.
            return ""+e.toString();
        }
    }
}