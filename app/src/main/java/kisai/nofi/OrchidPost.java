package kisai.nofi;

import java.io.Serializable;

public class OrchidPost implements Serializable {
    private String id_orchid_post, id_user, status, date_create;

    public OrchidPost(){}

    public OrchidPost(String id_orchid_post, String id_user, String status, String date_create) {
        this.id_orchid_post = id_orchid_post;
        this.id_user = id_user;
        this.status = status;
        this.date_create = date_create;
    }

    public String getId_orchid_post() {
        return id_orchid_post;
    }

    public void setId_orchid_post(String id_orchid_post) {
        this.id_orchid_post = id_orchid_post;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate_create() {
        return date_create;
    }

    public void setDate_create(String date_create) {
        this.date_create = date_create;
    }
}
