package kisai.nofi.Admin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import kisai.nofi.OrchidSpecies;
import kisai.nofi.R;
import kisai.nofi.listOrchid;

public class removeOrchidInfo extends AppCompatActivity {
    private DatabaseReference orchidDB;
    private FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orchid_remove);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            OrchidSpecies ocs = (OrchidSpecies) extras.get("ocs");
            Button confirm, cancel;
            confirm = (Button) findViewById(R.id.confirm);
            confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    remove(ocs);
                }
            });

            cancel = (Button) findViewById(R.id.cancel);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
    }
    private void remove(OrchidSpecies ocs){
        FirebaseDatabase.getInstance().getReference(getString(R.string.db_orchid)).child(ocs.getId_orchid_species()).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(removeOrchidInfo.this, getString(R.string.remove_success), Toast.LENGTH_SHORT).show();
                Intent listOrchidIntent = new Intent(removeOrchidInfo.this, listOrchid.class);
                listOrchidIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(listOrchidIntent);
                finish();
            }
        });
    }
}
