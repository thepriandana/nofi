package kisai.nofi.Admin;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import kisai.nofi.OrchidSpecies;
import kisai.nofi.R;
import kisai.nofi.dashboard;

public class createOrchidInfo extends AppCompatActivity {
    private int rcPermission, rcLocal;
    private Uri imageUri = null;
    private ImageView photo_iv;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orchid_create);

        rcPermission = Integer.parseInt(getString(R.string.RC_PERMISSION));
        rcLocal = Integer.parseInt(getString(R.string.RC_LOCAL));

        EditText name, desc;
        name = (EditText) findViewById(R.id.species);
        desc = (EditText) findViewById(R.id.description);

        Spinner branch, temperature, growth, fragance, sunlight;
        temperature = (Spinner) findViewById(R.id.temperature);
        branch = (Spinner) findViewById(R.id.branch);
        growth = (Spinner) findViewById(R.id.growth);
        fragance = (Spinner) findViewById(R.id.fragance);
        sunlight = (Spinner) findViewById(R.id.sunlight);

        ArrayAdapter<CharSequence> branchAdapter, temperatureAdapter, growthAdapter, fraganceAdapter, sunlightAdapter;
        branchAdapter = ArrayAdapter.createFromResource(this, R.array.crud_branch, android.R.layout.simple_spinner_item);
        branchAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        branch.setAdapter(branchAdapter);

        temperatureAdapter = ArrayAdapter.createFromResource(this, R.array.crud_temperature, android.R.layout.simple_spinner_item);
        temperatureAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        temperature.setAdapter(temperatureAdapter);

        growthAdapter = ArrayAdapter.createFromResource(this, R.array.crud_growthMedium, android.R.layout.simple_spinner_item);
        growthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        growth.setAdapter(growthAdapter);

        fraganceAdapter = ArrayAdapter.createFromResource(this, R.array.crud_fragance, android.R.layout.simple_spinner_item);
        fraganceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragance.setAdapter(fraganceAdapter);

        sunlightAdapter= ArrayAdapter.createFromResource(this, R.array.crud_sunlight, android.R.layout.simple_spinner_item);
        sunlightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sunlight.setAdapter(sunlightAdapter);

        photo_iv = (ImageView) findViewById(R.id.photo_view);
        Button select_photo = (Button) findViewById(R.id.photo);
        select_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findPhoto();
            }
        });
        Button submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseDatabase.getInstance().getReference(getString(R.string.db_orchid)).orderByChild("name_orchid").equalTo(name.getText().toString()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if(snapshot.exists()){
                            name.setError(getString(R.string.cu_exist_name));
                            name.requestFocus();
                            return;
                        }else{
                            if (name.getText().toString().equals("") || name.getText().toString().isEmpty()) {
                                name.setError(getString(R.string.cu_empty_name));
                                name.requestFocus();
                                return;
                            }else if (desc.getText().toString().equals("") || desc.getText().toString().isEmpty()) {
                                desc.setError(getString(R.string.cu_empty_desc));
                                desc.requestFocus();
                                return;
                            }else if(imageUri == null){
                                Toast.makeText(createOrchidInfo.this, getString(R.string.create_empty_image), Toast.LENGTH_SHORT).show();
                                return;
                            }else{
                                try {
                                    ProgressDialog todo = new ProgressDialog(createOrchidInfo.this);
                                    todo.setCancelable(false);
                                    todo.setMessage(getString(R.string.loading_wait));
                                    todo.show();
                                    String id_orchid = FirebaseDatabase.getInstance().getReference(getString(R.string.db_orchid)).push().getKey();
                                    final StorageReference fotoRef = FirebaseStorage.getInstance().getReference(getString(R.string.db_orchid_img)).child(id_orchid);
                                    fotoRef.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                            fotoRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                @Override
                                                public void onSuccess(Uri uri) {
                                                    OrchidSpecies newOrchid = new OrchidSpecies(id_orchid, name.getText().toString(),
                                                            uri.toString(),
                                                            desc.getText().toString(),
                                                            (fragance.getSelectedItemPosition() == 0),
                                                            growth.getSelectedItem().toString(),
                                                            branch.getSelectedItem().toString(),
                                                            temperature.getSelectedItem().toString(),
                                                            sunlight.getSelectedItem().toString());
                                                    FirebaseDatabase.getInstance().getReference(getString(R.string.db_orchid)).child(id_orchid).setValue(newOrchid).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {

                                                            todo.dismiss();
                                                            Toast.makeText(createOrchidInfo.this, getString(R.string.create_success), Toast.LENGTH_SHORT).show();
                                                            finish();
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }catch (Exception e){

                                }
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });
    }
    void findPhoto(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(createOrchidInfo.this,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(createOrchidInfo.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, rcPermission);
        }else{
            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, rcLocal);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == rcLocal && resultCode == RESULT_OK && data != null) {
            try {
                imageUri = data.getData();
                photo_iv.setImageURI(imageUri);
            }catch (Exception e){

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == rcPermission){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, rcLocal);
            } else {
                Toast.makeText(this, getString(R.string.error_permission), Toast.LENGTH_SHORT).show();
            }
        }
    }
}