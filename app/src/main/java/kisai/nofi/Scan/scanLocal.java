package kisai.nofi.Scan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import kisai.nofi.OrchidSpecies;
import kisai.nofi.R;
import kisai.nofi.TensorFlow.TensorClassifier;
import kisai.nofi.readOrchidInfo;
import kisai.nofi.sendFeedback;

public class scanLocal extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scan_local);
        TextView RTV1, RTV2, RTV3;
        ImageView localResult = (ImageView) findViewById(R.id.localResult);

        RTV1 = (TextView) findViewById(R.id.detected_item);
        RTV2 = (TextView) findViewById(R.id.detected_item1);
        RTV3 = (TextView) findViewById(R.id.detected_item2);

        if(getIntent().getExtras() != null){
            Intent intent = this.getIntent();
            Bundle bundle = intent.getExtras();
            localResult.setImageURI((Uri) bundle.get("resultImage"));
            ArrayList<TensorClassifier.Recognition> recognitions =
                    (ArrayList<TensorClassifier.Recognition>) bundle.getSerializable("resultList");
            Log.d("NOFI", recognitions.toString());
            if (recognitions != null && recognitions.size() >= 3) {
                TensorClassifier.Recognition recognition = recognitions.get(0);
                if (recognition != null) {
                    if (recognition.getTitle() != null){
                        Log.d("xxx", recognition.getTitle());
                        RTV1.setText(recognition.getTitle());
                        RTV1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openDetail(recognition.getTitle());
                            }
                        });
                    }
                }

                TensorClassifier.Recognition recognition1 = recognitions.get(1);
                if (recognition1 != null) {
                    if (recognition1.getTitle() != null) {
                        RTV2.setText(recognition1.getTitle());
                        RTV2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openDetail(recognition1.getTitle());
                            }
                        });
                    }
                }

                TensorClassifier.Recognition recognition2 = recognitions.get(2);
                if (recognition2 != null) {
                    if (recognition2.getTitle() != null) {
                        RTV3.setText(recognition2.getTitle());
                        RTV3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openDetail(recognition2.getTitle());
                            }
                        });
                    }
//                    if (recognition2.getConfidence() != null)
//                        RVTV3.setText(String.format("%.2f"));
                }

                //("%.2f", (100 * recognition2.getConfidence())
            }
            Button sf = (Button) findViewById(R.id.sendFeedback);
            sf.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
                    FirebaseUser account = firebaseAuth.getCurrentUser();
                    if(account!=null){
                        startActivity(new Intent(scanLocal.this, sendFeedback.class));
                    }else{
                        Toast.makeText(scanLocal.this, getString(R.string.feedback_sign), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
    private void openDetail(String orchid_name) {
        try {
            ProgressDialog progressDialog = new ProgressDialog(scanLocal.this);
            progressDialog.setMessage(getString(R.string.loading_wait));
            progressDialog.show();
            FirebaseDatabase.getInstance().getReference(getString(R.string.db_orchid)).orderByChild("name_orchid")
                    .equalTo(orchid_name).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()) {
                        ArrayList<OrchidSpecies> specieslist = new ArrayList<OrchidSpecies>();
                        for (DataSnapshot sn : snapshot.getChildren()) {
                            OrchidSpecies orchid = sn.getValue(OrchidSpecies.class);
                            specieslist.add(orchid);
                        }
                        progressDialog.dismiss();

                        Intent ro = new Intent(scanLocal.this, readOrchidInfo.class);
                        ro.putExtra("ocs", specieslist.get(0));
                        startActivity(ro);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        } catch (Exception e) {

        }
    }
}
