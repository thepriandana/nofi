package kisai.nofi.Scan;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.util.Size;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kisai.nofi.OrchidSpecies;
import kisai.nofi.R;
import kisai.nofi.TensorFlow.TensorClassifier;
import kisai.nofi.readOrchidInfo;

public class scanLive extends AppCompatActivity {
    private Camera mCamera;
    private CameraPreview mPreview;

    private boolean isScan = false;
    TextView RTV1, RTV2, RTV3;
    ImageView imageTest;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scan_live2);
        //check if the sdk is lolipop
        if (hasPermission()) {

            LinearLayout ll_logo = (LinearLayout) findViewById(R.id.ll_logo);
            ll_logo.bringToFront();

            RTV1 = (TextView) findViewById(R.id.detected_item);
            RTV2 = (TextView) findViewById(R.id.detected_item1);
            RTV3 = (TextView) findViewById(R.id.detected_item2);
            imageTest = (ImageView) findViewById(R.id.imtest);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setFragment();

            Button qx = (Button) findViewById(R.id.scan_now);
            qx.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!isScan){
                        qx.setText(getString(R.string.scan_live_stop));
                        isScan = true;
                        mCamera.setPreviewCallback(previewCb);
                        mCamera.startPreview();
                    }else{
                        qx.setText(getString(R.string.scan_live_start));
                        isScan = false;
                        mCamera.setPreviewCallback(null);
//                        mCamera.stopPreview();
                    }
                }
            });
        } else {
            requestPermission();
        }
    }
    /*
    * Make the fragment show the camera preview
    * */
    private void setFragment(){
        if(checkCameraHardware(scanLive.this) && Build.VERSION.SDK_INT >= 21){
            mCamera = getCameraInstance();
            // Create our Preview view and set it as the content of our activity.
            mPreview = new CameraPreview(this, mCamera, previewCb);
            FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
            preview.addView(mPreview);
        }else{
            Toast.makeText(this, getString(R.string.scan_live_notsupport), Toast.LENGTH_SHORT).show();
            finish();
        }
    }


    /*
    * Check if hardware has a camera
    * */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            return true; // this device has a camera
        } else {
            return false; // no camera on this device
        }
    }

    /*
    * Get camera id and try to open it
    * */
    public Camera getCameraInstance(){
        Camera c = null;
        try { c = Camera.open(getCameraId()); /* attempt to get a Camera instance*/ }
        catch (Exception e){
            Toast.makeText(this, getString(R.string.scan_live_cameraerror), Toast.LENGTH_SHORT).show();
            finish();
        }
        return c; // returns null if camera is unavailable
    }
    private int getCameraId() {
        Camera.CameraInfo ci = new Camera.CameraInfo();
        for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
            Camera.getCameraInfo(i, ci);
            if (ci.facing == Camera.CameraInfo.CAMERA_FACING_BACK) return i;
        }
        return -1; // No camera found
    }




    /*
     * check the permision
     */
    private static final String PERMISSION_CAMERA = Manifest.permission.CAMERA;
    private static final int PERMISSIONS_REQUEST = 1;
    public void onRequestPermissionsResult(final int requestCode, final String[] permissions, final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_REQUEST) {
            if (allPermissionsGranted(grantResults)) {
                setFragment();
            } else {
                requestPermission();
            }
        }
    }
    private static boolean allPermissionsGranted(final int[] grantResults) {
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }
    private boolean hasPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(PERMISSION_CAMERA) == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }
    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (shouldShowRequestPermissionRationale(PERMISSION_CAMERA)) {
                Toast.makeText(
                        scanLive.this,
                        "Camera permission is required for this demo",
                        Toast.LENGTH_LONG)
                        .show();
            }
            requestPermissions(new String[] {PERMISSION_CAMERA}, PERMISSIONS_REQUEST);
        }
    }


    Camera.PreviewCallback previewCb = (data, camera) -> {
        try {
            Camera.Parameters parameters = camera.getParameters();
            int width = parameters.getPreviewSize().width;
            int height = parameters.getPreviewSize().height;

            YuvImage yuv = new YuvImage(data, parameters.getPreviewFormat(), width, height, null);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            yuv.compressToJpeg(new Rect(0, 0, width, height), 100, out);
            byte[] bytes = out.toByteArray();
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
//            imageTest.setImageBitmap(bitmap);

            TensorClassifier classifier = TensorClassifier.create(this, TensorClassifier.Device.CPU, 2);
            ArrayList<TensorClassifier.Recognition> recognitions = classifier.recognizeImage(bitmap, 90 - getScreenOrientation());
            if (recognitions != null && recognitions.size() >= 3) {
                setRecognitionResult(recognitions);
            }
        }catch (Exception e){

        }

    };
    private int getScreenOrientation() {
        switch (getWindowManager().getDefaultDisplay().getRotation()) {
            case Surface.ROTATION_270:
                return 270;
            case Surface.ROTATION_180:
                return 180;
            case Surface.ROTATION_90:
                return 90;
            default:
                return 0;
        }
    }

    private void setRecognitionResult( ArrayList<TensorClassifier.Recognition> recognitions ){
            if (recognitions != null && recognitions.size() >= 3) {
                TensorClassifier.Recognition recognition = recognitions.get(0);
                if (recognition != null) {
                    if (recognition.getTitle() != null) {
                        RTV1.setText(recognition.getTitle());
                        RTV1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openDetail(recognition.getTitle());
                            }
                        });
                    }
                }

                TensorClassifier.Recognition recognition1 = recognitions.get(1);
                if (recognition1 != null) {
                    if (recognition1.getTitle() != null) {
                        RTV2.setText(recognition1.getTitle());
                        RTV2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openDetail(recognition1.getTitle());
                            }
                        });
                    }
                }

                TensorClassifier.Recognition recognition2 = recognitions.get(2);
                if (recognition2 != null) {
                    if (recognition2.getTitle() != null) {
                        RTV3.setText(recognition2.getTitle());
                        RTV3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openDetail(recognition2.getTitle());
                            }
                        });
                    }
                }
            }
    }

    private void openDetail(String orchid_name) {
        try {
            ProgressDialog progressDialog = new ProgressDialog(scanLive.this);
            progressDialog.setMessage(getString(R.string.loading_wait));
            progressDialog.show();
            FirebaseDatabase.getInstance().getReference(getString(R.string.db_orchid)).orderByChild("name_orchid")
                    .equalTo(orchid_name).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()) {
                        ArrayList<OrchidSpecies> specieslist = new ArrayList<OrchidSpecies>();
                        for (DataSnapshot sn : snapshot.getChildren()) {
                            OrchidSpecies orchid = sn.getValue(OrchidSpecies.class);
                            specieslist.add(orchid);
                        }
                        progressDialog.dismiss();

                        Intent ro = new Intent(scanLive.this, readOrchidInfo.class);
                        ro.putExtra("ocs", specieslist.get(0));
                        startActivity(ro);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        } catch (Exception e) {

        }
    }
}

class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder mHolder;
    private Camera mCamera;
    Camera.PreviewCallback previewCallback;
    public CameraPreview(Context context, Camera camera, Camera.PreviewCallback previewCallback) {
        super(context);
        mCamera = camera;
        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        this.previewCallback = previewCallback;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            Camera.Parameters parameters = mCamera.getParameters();
            List<String> focusModes = parameters.getSupportedFocusModes();
            if (focusModes != null
                    && focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            }
            List<Camera.Size> cameraSizes = parameters.getSupportedPreviewSizes();
            Size[] sizes = new Size[cameraSizes.size()];
            int i = 0;
            for (Camera.Size size : cameraSizes) {
                sizes[i++] = new Size(size.width, size.height);
            }
            mCamera.setDisplayOrientation(90);
            mCamera.setParameters(parameters);
            mCamera.setPreviewDisplay(holder);
        } catch (IOException e) {
            mCamera.release();
            //Log.d(TAG, "Error setting camera preview: " + e.getMessage());
        }
        mCamera.setPreviewCallbackWithBuffer(previewCallback);
        Camera.Size s = mCamera.getParameters().getPreviewSize();
        mCamera.startPreview();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // empty. Take care of releasing the Camera preview in your activity.
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.
        if (mHolder.getSurface() == null){
            return; // preview surface does not exist
        }
        try {
            mCamera.stopPreview(); // stop preview before making changes
        } catch (Exception e){
            mCamera.release();
        }
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();
        } catch (Exception e){
            mCamera.release();
            //Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }
    }
}
