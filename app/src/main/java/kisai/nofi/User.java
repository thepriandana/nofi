package kisai.nofi;

import java.io.Serializable;

public class User implements Serializable {
    private String user_id;
    private String email;
    private String name_user;
    private String level;
    private String date_create;

    public User(){}
    public User(String uid, String email, String name, String level, String date){
        this.user_id = uid;
        this.email = email;
        this.name_user = name;
        this.level = level;
        this.date_create = date;
    }

    public String getDate_create() { return date_create; }

    public void setDate_create(String date_create) { this.date_create = date_create; }

    public String getName_user() { return name_user;}

    public void setName_user(String name_user) { this.name_user = name_user; }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name_user;
    }

    public void setName(String name) {
        this.name_user = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
