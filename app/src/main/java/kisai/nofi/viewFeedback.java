package kisai.nofi;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import kisai.nofi.Adapter.feedbackAdapter;

public class viewFeedback extends AppCompatActivity {
    ArrayList<OrchidPost> fblist;
    ArrayList<ArrayList<OrchidImage>> orchidImageList;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback_list);

        orchidImageList = new ArrayList<ArrayList<OrchidImage>>();
        fblist = new ArrayList<OrchidPost>();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference userDB = firebaseDatabase.getReference(getString(R.string.db_user));
            userDB.child(user.getUid()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    User current = snapshot.getValue(User.class);
                    if (current.getLevel().equals("admin")) {
                        check_orchid(true);
                    }else{
                        check_orchid(false);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }else{
            check_orchid(false);
        }
    }
    void check_orchid(boolean isAdmin){
        DatabaseReference fbDB = FirebaseDatabase.getInstance().getReference(getString(R.string.db_feedback_post));
        if(!isAdmin){
            fbDB.orderByChild("id_user").equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    listing(snapshot);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }else{
            fbDB.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    listing(snapshot);
                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
    }

    void listing(DataSnapshot snapshot){
        LinearLayout ll_feedback = (LinearLayout) findViewById(R.id.ll_feedback);
        ListView fb_lv = (ListView) findViewById(R.id.fs_list);
        for (DataSnapshot sn : snapshot.getChildren()) {
            OrchidPost op = sn.getValue(OrchidPost.class);
            fblist.add(op);
            FirebaseDatabase.getInstance().getReference(getString(R.string.db_feedback_img))
                    .orderByChild("id_orchid_post").equalTo(op.getId_orchid_post())
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot1) {
                            ArrayList<OrchidImage> tmp = new ArrayList<OrchidImage>();
                            for (DataSnapshot sn2 : snapshot1.getChildren()) {
                                OrchidImage orchidImage = sn2.getValue(OrchidImage.class);
                                tmp.add(orchidImage);
                            }
                            orchidImageList.add(tmp);
                            fb_lv.setAdapter(new feedbackAdapter(viewFeedback.this, fblist, orchidImageList));
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error1) {

                        }
                    });
            if(fblist.isEmpty()){
                fb_lv.setVisibility(View.GONE);
                ll_feedback.setVisibility(View.VISIBLE);
            }else{
                ll_feedback.setVisibility(View.GONE);
                fb_lv.setVisibility(View.VISIBLE);
            }
        }
    }
}
