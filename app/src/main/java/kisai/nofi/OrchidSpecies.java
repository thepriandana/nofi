package kisai.nofi;

import java.io.Serializable;

public class OrchidSpecies implements Serializable {

    private String id_orchid_species, name_orchid, imageUri, description, growthMedium, branchType, temperature, sunlight;
    private boolean fragrance;

    public OrchidSpecies(){}

    public OrchidSpecies(String id_orchid_species, String name_orchid, String imageUri, String description, boolean fragrance, String growthMedium, String branchType, String temperature, String sunlight) {
        this.id_orchid_species = id_orchid_species;
        this.name_orchid = name_orchid;
        this.imageUri = imageUri;
        this.description = description;
        this.fragrance = fragrance;
        this.growthMedium = growthMedium;
        this.branchType = branchType;
        this.temperature = temperature;
        this.sunlight = sunlight;
    }

    public String getName_orchid() {
        return name_orchid;
    }

    public void setName_orchid(String name_orchid) {
        this.name_orchid = name_orchid;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getFragrance() {
        return fragrance;
    }

    public void setFragrance(boolean fragrance) {
        this.fragrance = fragrance;
    }

    public String getGrowthMedium() {
        return growthMedium;
    }

    public void setGrowthMedium(String growthMedium) {
        this.growthMedium = growthMedium;
    }

    public String getBranchType() {
        return branchType;
    }

    public void setBranchType(String brachType) {
        this.branchType = brachType;
    }

    public String getSunlight() {
        return sunlight;
    }

    public void setSunlight(String sun) {
        this.sunlight = sun;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getId_orchid_species() {
        return id_orchid_species;
    }

    public void setId_orchid_species(String id_orchid_species) { this.id_orchid_species = id_orchid_species; }
}
