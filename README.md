# NOFI - Neural Orchid Finder

![GitLab License](https://img.shields.io/gitlab/license/thepriandana/NOFI)
## Description

NOFI (Neural Orchid Finder) is an Android application that use TensorFlow Lite models to accurately identify a wide range of orchid species. The application has been designed to be user-friendly, efficient, and effective. Currently, it can classify 52 different orchid species by their petals with an impressive accuracy of 92-96%.

## Features

- Identify 52 unique orchid species instantly using your Android device's camera.
- High accuracy detection ranging from 92-96% through our refined TensorFlow Lite model.
- Friendly user interface for all users.

## Installation

1. Clone this repository:

    ```bash
    git clone https://gitlab.com/thepriandana/NOFI.git
    ```

2. Open the project in your favorite Android development environment (recommended: Android Studio)

3. Build the project and run it on your Android device.

## Usage

1. Open the NOFI application on your Android device.
2. Grant necessary permissions (like camera access).
3. Point the camera towards the orchid you want to identify.
4. Get the information about the orchid species instantly.

## License

This project is licensed under the GPLv3 License.
